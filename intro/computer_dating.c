#include <time.h>
#include <stdio.h>
#include <stdlib.h>

// time_t is a long int variable. On x64, it is coded with 64 bits.

int main() {
    time_t maxtime = 0xF0C29D868C43CF; // Ends at Dec 31 23:59:59 2 147 483 647.
    // On 32 bits systems, ends at 0x7FFFFFFF. (Since its a signed integer)
    printf("%s", ctime(&maxtime));
    return 0;
}
